https://medium.com/4thought-studios/documenting-javascript-projects-f72429da2eea
https://github.com/documentationjs/documentation#documentation
https://developers.bigcartel.com/api/v1#introduction

Usage:

# generate markdown docs for index.js and files it references
documentation build index.js -f md

# generate html docs for all files in src
documentation build src/** -f html -o docs

# document index.js, ignoring any files it requires or imports
documentation build index.js -f md --shallow

# build and serve HTML docs for app.js
documentation serve app.js

# build, serve, and live-update HTML docs for app.js
documentation serve --watch app.js

# validate JSDoc syntax in util.js
documentation lint util.js

# update the API section of README.md with docs from index.js
documentation readme index.js --section=API

# build docs for all values exported by index.js
documentation build --document-exported index.js

Commands:
  serve [input..]   generate, update, and display HTML documentation
  build [input..]   build documentation
  lint [input..]    check for common style and uniformity mistakes
  readme [input..]  inject documentation into your README.md

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]